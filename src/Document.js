const Document = ({
    title,
    content
}) => {
    const handleScroll = (e) => {
        console.log(e.target.scrollTop);
        const bottom = e.target.scrollHeight - e.target.scrollTop;
        if (e.target.scrollTop > 800) { 
            console.log("bottom");
            document.querySelector('.button').disabled = false;
         }
      }
    

    return(
        <div>
            <h1 className="title">{title}</h1>
            <p onScroll={handleScroll} className="content">{content}</p>
            <button className="button" disabled={true}>I Agree</button>
        </div>
    );
}

export default Document;